<?php
  include "header.php";

 ?>
 <head>
   <link rel="stylesheet" href="../css/plus.css">
 </head>


<div class="recipiente">

 <section class="panel" style="margin-top: 300px;">
   <table>
     <tr> <td> <a href="../admin/perfil.php" onclick="perfils(\'perfil\')">Perfil</a> </td> </tr>
     <tr> <td> <a href="../admin/own_bdl.php" onclick="perfils(\'msjs\')"> Mis bundles</a> </td> </tr>
     <tr> <td> <a href="../admin/ordenes.php" onclick="perfils(\'orders\')" >Ordenes</a> </td> </tr>
     <tr> <td> <a href="../admin/payment.php" onclick="perfils(\'pago\')" >Métodos de pago</a> </td> </tr>
   </table>
 </section>

 <section class="contenido" style="margin-top: 110px;">
   <form class="" action="../admin/perfil.php" method="post">
   <?php
    include "../class/classBundles.php";
    $registros=$oBD->saca_tupla("select * from pedido where id_cliente_fk_2=".$_SESSION['id']);
   ?>
   <div class="titulo"><span class="title-p">Ordenes</span></div>
   <h4 style="margin: 50px 0 20px 50px;">Mis pedidos realizados</h4>
   <div id="contenido">
   <?php
   echo $oBD->desplegarTabla("select id_pedido as ID, fecha_pedido as 'Fecha pedido', fecha_entrega as 'Fecha entrega', nombre_bdl as 'Bundle', tipo_pago as 'Tipo pago'
              from pedido p join cliente c on p.id_cliente_fk_2=c.id_cliente
                            join bundles b on p.id_bdl_fk=b.id_bdl
                            join tipo_pago t on p.id_tipo_pago_fk=t.id_tipo_pago
              where id_cliente_fk_2=".$_SESSION['id'], array('details','delete_d'));

    ?> </div>
    <h4 style="margin: 50px 0 20px 50px;">Pedidos recibidos</h4>
    <div id="contenido">
    <?php
    echo $oBD->desplegarTabla("select id_pedido as ID, fecha_pedido as 'Fecha pedido', fecha_entrega as 'Fecha entrega', nombre_cliente as 'Nombre cliente', nombre_bdl as 'Bundle', tipo_pago as 'Tipo pago'
               from pedido p join cliente c on p.id_cliente_fk_2=c.id_cliente
                             join bundles b on p.id_bdl_fk=b.id_bdl
                             join tipo_pago t on p.id_tipo_pago_fk=t.id_tipo_pago
               where id_bdl_fk in (select id_bdl
                                     from bundles bd join cliente cl on bd.id_cliente_fk=cl.id_cliente
                                       where id_cliente=".$_SESSION['id'].")", array('details','delete_d'));

     ?> </div>
  </form>
</section>
</div>
</body>
</html>

<script src="../controllers/ordenes.js"></script>
<!--vista vendedor
echo $oBD->desplegarTabla("select id_pedido as ID, fecha_pedido as 'Fecha pedido', fecha_entrega as 'Fecha entrega', concat(nombre_cliente,' ', apaterno_cliente,' ', amaterno_cliente) as 'Nombre cliente', nombre_bdl as 'Bundle', tipo_pago as 'Tipo pago'
           from pedido p join cliente c on p.id_cliente_fk_2=c.id_cliente
                         join bundles b on p.id_bdl_fk=b.id_bdl
                         join tipo_pago t on p.id_tipo_pago_fk=t.id_tipo_pago
           where id_bdl_fk in (select id_bdl
                                 from bundles bd join cliente cl on bd.id_cliente_fk=cl.id_cliente
                                   where id_cliente=".$_SESSION['id'].")", array('details','delete_d'));
-->
