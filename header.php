<?php
  session_start();
if (!isset($_SESSION['user'])) {
  header("location: ../index.php?m=100");
}
?>

<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Inicio</title>
    <link rel="stylesheet" href="css/estilos.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/a81368914c.js"></script>
    <link rel="stylesheet" href="css/bootstrap.css">
  </head>
  <body>
  <?php include "nav_pg0.php"; ?>
