<?php

if (!isset($_SESSION['user'])) {
  session_start();
} else {
  if (!isset($_SESSION['user'])) {
    header("location: ../index.php?m=100");
  }
}

  include "../class/classBaseDatos.php";
class classBundles extends BaseDatos{
var $queryConsulta;
function classBundles(){
  $this->queryConsulta="select id_bdl, nombre_bdl, descripcion_bdl, precio_bdl, tematica, color, categoria from bundles bd join tematica t on bd.id_tematica_fk=t.id_tematica join color c on bd.id_color_fk=c.id_color join categoria g on bd.id_categoria_fk=g.id_categoria where id_cliente_fk=".$_SESSION['id'];
}

  function action($pAccion){
  switch ($pAccion) {
    case 'delete':
      $this->consulta("delete from bundles where id_bdl=".$_GET['id_bdl']);
      //echo $this->desplegarTabla($this->queryConsulta, array("update","delete","addPreg","vista"),array(10,20,40,20));
      break;
    case 'formupdate':

       $registros=$this->saca_tupla("select * from bundles where id_bdl=".$_GET['identi']);

    case 'add':
      echo '<div class="container">
            <form method="post" id="formBundles">
            <input type="hidden" name="accion" value="'.(isset($registros->id_bdl)?"update":"insert").'"/>';

            if(isset($registros->id_bdl)){//si el registro existe
              //entonces es un formulario existente
              echo '<input type="hidden" name="id_cliente_fk" value="'.$registros->id_cliente_fk.'"/>';//cambiar a id_bdl?
              echo '<input type="hidden" name="id_bdl" value="'.$registros->id_bdl.'"/>';
            }else {
              //sino es un form nuevo
              echo '<input type="hidden" name="id_cliente_fk" value="'.$_SESSION['id'].'"/>';
            }

        echo '
            <div class="row">
              <label for="" class="col-md-4">Nombre</label>
              <div class="col-md-8">
                <input type="text" class="form-control" name="nombre_bdl" value="'.(isset($registros->id_bdl)?$registros->nombre_bdl:"").'"/>
              </div>
            </div>

            <div class="row">
              <label for="" class="col-md-4">Descripcion</label>
              <div class="col-md-8">
                <textarea class="form-control" name="descripcion_bdl">'.(isset($registros->id_bdl)?$registros->descripcion_bdl:"").'</textarea>
              </div>
            </div>

            <div class="row">
              <label for="" class="col-md-4">Precio</label>
              <div class="col-md-8">
                <input type="text" class="form-control" name="precio_bdl" value="'.(isset($registros->id_bdl)?$registros->precio_bdl:"").'"/>
              </div>
            </div>

            <div class="row">
              <label for="" class="col-md-4"> Tematica</label>
              <div class="col-md-8">';
              echo  $this->creaSELECT("tematica","id_tematica","tematica","id_tematica_fk", isset($registros->id_tematica)?$registros->id_tematica:-1);
              echo '
            </div>
            <div class="row">
              <label for="" class="col-md-4">Color</label>
              <div class="col-md-8">';
              echo  $this->creaSELECT("color","id_color","color","id_color_fk", isset($registros->id_color)?$registros->id_color:-1);
              echo '
            </div>
            <div class="row">
              <label for="" class="col-md-4">Categoria</label>
              <div class="col-md-8">';
              echo  $this->creaSELECT("categoria","id_categoria","categoria","id_categoria_fk", isset($registros->id_categoria)?$registros->id_categoria:-1);
              echo '
            </div>

            </form>
            </div>
      ';

      break;

      case 'update':
      $query="update bundles set ";
      foreach ($_POST as $nombCampo => $valor) {
        if(!in_array($nombCampo, array("accion","id_bdl"))){
          $query.=$nombCampo."='".$valor."', ";}
      }

      $query=substr($query,0,-2);
      $query.=" where id_bdl=".$_GET['id_bdl'];
      $this->consulta($query);
        //echo $this->desplegarTabla($this->queryConsulta, array("update","delete","addPreg","vista"),array(10,20,40,20));
      break;
    case 'insert':
      $query="insert into bundles set ";
      foreach ($_POST as $nombCampo => $valor) {
        if(!in_array($nombCampo, array("accion"))){
          $query.=$nombCampo."='".$valor."', ";}
        //echo $query;
      }
      $query=substr($query,0,-2);
      $this->consulta($query);
      echo $this->desplegarTabla("select id_bdl as 'ID', nombre_bdl as 'Nombre',descripcion_bdl as 'Descripcion', precio_bdl as 'Precio', tematica as 'Tematica', color as 'Color', categoria as 'Categoria' from bundles bd join tematica t on bd.id_tematica_fk=t.id_tematica join color c on bd.id_color_fk=c.id_color join categoria g on bd.id_categoria_fk=g.id_categoria where id_cliente_fk=".$_SESSION['id'], array('update','delete'));
      break;

      case "list":
        echo $this->desplegarTabla("select id_bdl as 'ID', nombre_bdl as 'Nombre',descripcion_bdl as 'Descripcion', precio_bdl as 'Precio', tematica as 'Tematica', color as 'Color', categoria as 'Categoria' from bundles bd join tematica t on bd.id_tematica_fk=t.id_tematica join color c on bd.id_color_fk=c.id_color join categoria g on bd.id_categoria_fk=g.id_categoria where id_cliente_fk=".$_SESSION['id'], array('update','delete'));
        break;

      case 'mostrar':
      $registros=$this->saca_tupla("select *,tematica from bundles b join tematica t on b.id_tematica_fk=t.id_tematica where id_bdl=".$_GET['id_bdl']);
        echo '
        <table id="tbm">
          <tr>
            <td id="nombreBDL">'.$registros->nombre_bdl.'</td>
            <td id="tema"><span class="bdg">'.$registros->tematica.'</span></td>
          </tr>
        </table>
        <div class="view-data">
          <p>'.$registros->descripcion_bdl.'</p>
          <span> $'.$registros->precio_bdl.' MXN</span>
        </div>
        ';
        break;

        case 'venta':
        $registros=$this->saca_tupla("select *,tematica from bundles b join tematica t on b.id_tematica_fk=t.id_tematica join cliente c on c.id_cliente=b.id_cliente_fk where id_bdl=".$_GET['id_bdl']);

          echo '
          <div class="view-data" style="margin-top: 20px;">
            <p><span>Nombre del paquete:</span> '.$registros->nombre_bdl.'</p
            <p><span>Temática:</span> '.$registros->tematica.'</p>
            <p><span>Precio:</span> $'.$registros->precio_bdl.' MXN</p>
            <p><span>Vendedor:</span> '.$registros->nombre_usuario.'</p>
            <form id="formVista" method="post">
            <p><span>Fecha de entrega:</span> <input type="date" name="fecha_entrega"/></p>
            <p><span>Método de pago:</span>';
            echo  $this->creaSELECT("tipo_pago","id_tipo_pago","tipo_pago","id_tipo_pago_fk", isset($registros->id_tipo_pago)?$registros->id_tipo_pago:-1);
            echo '</p>
            </form>
          </div>
          ';
          break;
        case 'comprar':
        $query="insert into pedido set fecha_pedido=now(), fecha_entrega='".$_POST['fecha_entrega']."', cantidad=1, id_cliente_fk_2=".$_SESSION['id'].", id_bdl_fk=".$_GET['identi'].", id_tipo_pago_fk=".$_POST['id_tipo_pago_fk'];

        $this->consulta($query);
          break;
    //default: echo $this->desplegarTabla("select * from encuesta", array("update","delete"),array(10,20,40,20));
  }
}//fin function action

}//fin classBundles

if(isset($_REQUEST['accion'])){
  $objeto=new classBundles();
  $objeto->action($_REQUEST['accion']);
}
?>
