<?php

if (!isset($_SESSION['user'])) {
  session_start();
} else {
  if (!isset($_SESSION['user'])) {
    header("location: ../index.php?m=100");
  }
}

  include "../class/classBaseDatos.php";
class classOrdenes extends BaseDatos{
var $queryConsulta;
function classOrdenes(){
  $this->queryConsulta="select * from pedido where id_cliente_fk_2=".$_SESSION['id'];
}

  function action($pAccion){
  switch ($pAccion) {
    case 'delete':
      $this->consulta("delete from pedido where id_pedido=".$_GET['id_pedido']);
      //echo $this->desplegarTabla($this->queryConsulta, array("update","delete","addPreg","vista"),array(10,20,40,20));
      break;


      case "list":
      echo $this->desplegarTabla("select id_pedido as ID, fecha_pedido as 'Fecha pedido', fecha_entrega as 'Fecha entrega', nombre_cliente as 'Nombre cliente', nombre_bdl as 'Bundle', tipo_pago as 'Tipo pago'
                 from pedido p join cliente c on p.id_cliente_fk_2=c.id_cliente
                               join bundles b on p.id_bdl_fk=b.id_bdl
                               join tipo_pago t on p.id_tipo_pago_fk=t.id_tipo_pago
                 where id_cliente_fk_2=".$_SESSION['id'], array('details','delete_d'));
      break;

      case 'detalles':
      $registros=$this->saca_tupla("select  *
      from pedido p join cliente c on p.id_cliente_fk_2=c.id_cliente
                    join bundles b on p.id_bdl_fk=b.id_bdl
                    join tipo_pago t on p.id_tipo_pago_fk=t.id_tipo_pago
                    join tematica tm on b.id_tematica_fk=tm.id_tematica
                    join categoria cat on b.id_categoria_fk=cat.id_categoria
                    join color co on b.id_color_fk=co.id_color
      where id_pedido=".$_GET['id_pedido']);

        echo '
        <div class="view-data" style="margin-top: 5px;">
        <h5>Cliente</h5>
          <table id="tbm" class="tb-dt">
            <tr> <td><span style="font-size: 16px;">Nombre</span></td>  <td id="col-datos">'.$registros->nombre_cliente,' ',$registros->apaterno_cliente,' ',$registros->amaterno_cliente.'</td>  </tr>
            <tr> <td><span style="font-size: 16px;">Dirección</span></td>  <td id="col-datos">'.$registros->direccion_cliente.'</td>  </tr>
            <tr> <td><span style="font-size: 16px;">Colonia</span></td>  <td id="col-datos">'.$registros->colonia_cliente.'</td>  </tr>
            <tr> <td><span style="font-size: 16px;">C.P.</span></td>  <td id="col-datos">'.$registros->cp_cliente.'</td>  </tr>
            <tr> <td><span style="font-size: 16px;">Telefono</span></td>  <td id="col-datos">'.$registros->telefono_cliente.'</td>  </tr>
          </table>
        <h5 style="margin-top: 15px;">Bundle</h5>
          <table id="tbm" class="tb-dt">
            <tr> <td><span style="font-size: 16px;">Paquete</span></td>  <td id="col-datos">'.$registros->nombre_bdl.'</td>  </tr>
            <tr> <td><span style="font-size: 16px;">Precio</span></td>  <td id="col-datos">$'.$registros->precio_bdl.' MXN</td>  </tr>
            <tr> <td><span style="font-size: 16px;">Categoria</span></td>  <td id="col-datos">'.$registros->categoria.'</td>  </tr>
            <tr> <td><span style="font-size: 16px;">Tematica</span></td>  <td id="col-datos">'.$registros->tematica.'</td>  </tr>
          </table>
        </div>
        ';
        break;
    //default: echo $this->desplegarTabla("select * from encuesta", array("update","delete"),array(10,20,40,20));
  }
}//fin function action

}//fin classBundles

if(isset($_REQUEST['accion'])){
  $objeto=new classOrdenes();
  $objeto->action($_REQUEST['accion']);
}
?>
