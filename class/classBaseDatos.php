<?php

class BaseDatos{
  var $conexion;
  var $error;
  var $numeRegistros;

function conecta(){
  $this->conexion=mysqli_connect("localhost:3307", "root", '','fiestas');

}
  function cierraBD(){mysqli_close($this->conexion);}


  function consulta($query){
    $this->conecta();
    $bloque=mysqli_query($this->conexion,$query);
    if (strpos( strtolower($query), "select")!==false) {
      $this->numeRegistros=mysqli_num_rows($bloque);
    } else {
      $this->numeRegistros=0;
    }

    $this->error=mysqli_error($this->conexion);
    if($this->error>""){
      echo $query," => ".$this->error;
      exit;
    }

    $this->cierraBD();
    return $bloque;
  }


  function saca_tupla($query){
    $this->conecta();
    $bloque=mysqli_query($this->conexion,$query);
    $this->numeRegistros=mysqli_num_rows($bloque);
    $this->cierraBD();
    return mysqli_fetch_object($bloque);
  }



function desplegarTabla($query,$iconos=array(),$anchoC=array(),$coloTabla="table table-hover"){

  $registros=$this->consulta($query);
  $columnas=mysqli_num_fields($registros);
  $result= '<div><table class="table '.$coloTabla.'">';
  $result.= '<thead>';
  //cabecera
  $result.= '<tr>';
    //iconos
    if (count($iconos)) {
      //foreach ($iconos as $icono) {
        $result.= '<th scope="col" colspan="'.count($iconos).'">
        <img src="../icon/add.png" class="iconos" id="hid" onclick="bundless(\'add\')">
        </th>';
      //}
    }
    //fin iconos
    if (empty($anchoC)) {
      for ($c=0; $c <$columnas ; $c++){
        $campos=mysqli_fetch_field_direct($registros,$c);
        $result.= '<th scope="col">'.$campos->name.'</td>';
      }
    } else {
      for ($c=0; $c <$columnas ; $c++){
        $campos=mysqli_fetch_field_direct($registros,$c); //da la informacion de un campo(llave primaria,nombre del campo, longitud del campo, tipo de dato del campo), de una consulta que hicimos
        $result.= '<td style="width:'.$anchoC[$c].'%;">'.$campos->name.'</td>';
      }
    }
  $result.= '</tr></thead>';
  //fin cabecera
  for ($r=0; $r<$this->numeRegistros ; $r++) {
    $result.= '<tr class="table-active">';

    $campos=mysqli_fetch_array($registros);
    //incorporar inconos
    if (in_array("details",$iconos)) {
      $result.= '<td class="td-icon" style="width:5%;">
      <img class="iconos" src="../icon/details.png" onclick="detailss(\'detalles\','.$campos['ID'].')">
      </td>';
    }
    if (in_array("delete_d",$iconos)) {
      $result.= '<td class="td-icon" style="width:5%;">
      <img src="../icon/borrar.png" class="iconos" onclick="detailss(\'delete\','.$campos['ID'].')">
      </td>';

    }
    if (in_array("update",$iconos)) {
      $result.= '<td class="td-icon" style="width:5%;">
      <img src="../icon/editar.png" class="iconos" onclick="bundless(\'formupdate\','.$campos['ID'].')">
      </td>';
    }
    if (in_array("delete",$iconos)) {
      $result.= '<td class="td-icon" style="width:5%;">
      <img src="../icon/borrar.png" class="iconos" onclick="bundless(\'delete\','.$campos['ID'].')">
      </td>';

    }
    if (in_array("addPreg",$iconos)) {
      $result.= '<td class="td-icon" style="width:10%;">
      <img src="../icon/addp.png" class="iconos" onclick="preguntas(\'list\','.$campos['id'].')">
      </td>';
    }
    if (in_array("vista",$iconos)) {
      $result.= '<td class="td-icon" style="width:5%;">
      <img class="iconos" src="../icon/vist.png" onclick="bundless(\'mostrar\','.$campos['ID'].')">
      </td>';
    }
    //fin incorporar iconos

    if (empty($anchoC)) {
      for ($c=0; $c <$columnas ; $c++)
        $result.= '<td scope="col">'.$campos[$c].'</td>';
    } else {
      for ($c=0; $c <$columnas ; $c++)
        $result.= '<td style="width:'.$anchoC[$c].'%;">'.$campos[$c].'</td>';
    }
    $result.= '</tr>';
  }
  $result.= '</tbody>';
  $result.= '</table></div>';
  return $result;
}


public function creaSELECT ($tabla, $PK, $campDesplegar, $nameSelect, $IdSeleccionado=0){
  $cad="select ".$PK." as PK, ".$campDesplegar." as dato from ".$tabla." order by ".$campDesplegar;
  $registros=$this->consulta($cad);
  $result='<select class="form-control" name="'.$nameSelect.'">';
  $result.='<option value="0" disabled>----Selecciona----</option>';
  foreach ($registros as $registros) {
    $result.='<option value="'.$registros['PK'].'" '.(($IdSeleccionado==$registros['PK'])?" selected ":"").' >'.$registros['dato'].'</option>';
  }
  $result.='</select>';
  return $result;
}


}

$oBD=new BaseDatos();
?>
