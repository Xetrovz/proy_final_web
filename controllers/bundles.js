var param;
function bundless (accion, id_bdl) {

  switch(accion){
    case 'add': $.confirm({
        title: 'Nuevo bundle',
        content: 'url: ../class/classBundles.php?accion='+accion,
        columnClass: 'large',
        type: 'green',
        buttons: {
          aceptar: {
            text: 'Registrar',
            btnClass: 'btn-green',
            action: function() {
              bundless('insert'); }},
            cancel: function (){
               } }});
  break;
  case 'list':
      $.ajax({
        url: "../class/classBundles.php",
        data: {'accion':'list'},
        type: "POST",
        success: function (result){
          contenido.innerHTML=result;
        }
      })
  break;
  case 'insert':
    $datos=$("#formBundles").serialize();
      $.ajax({
        url: "../class/classBundles.php",
        data: $datos,
        type: "POST",
        beforeSend: function(){ contenido.innerHTML='Cargando...'},
        success: function (result){
          $.alert({
            title: "Atencion",
            content: "Se inserto el registro",
            type: 'green'
          })//fin alert
          bundless('list');
        },
      });
  break;
  case  'delete':
  $.confirm({
     title: 'Borrar',
      content: '¿Desea borrar este elemento?',
      columnClass: 'small',
      type: 'red',
      buttons: {
        borrar: {
          text: 'Borrar',
          btnClass: 'btn-red',
          action: function() {
           $.confirm({title: 'Eliminado',
                    content: 'url: ../class/classBundles.php?accion='+accion+'&id_bdl='+id_bdl,
                    columnClass: 'xsmall',
                    type: 'orange',
                    buttons:{
                      aceptar: {
                        text: 'Aceptar',
                        action: function () {
                          bundless('list');
                        }
                      }
                    }
                  });
        }},
          cancel: function (){
           $.alert('Acción abortada'); } }});
    break;

      case 'formupdate':
      param=id_bdl;
            $.confirm({
                title: 'Editar bundle',
                content: 'url: ../class/classBundles.php?accion='+accion+"&identi="+id_bdl,
                columnClass: 'large',
                type: 'orange',
                buttons: {
                  aceptar: {
                    text: 'Guardar',
                    btnClass: 'btn-orange',
                    action: function() {
                      bundless('update'); }},
                    cancel: function (){
                     } }});
        break;

        case 'update':
        $datos=$("#formBundles").serialize();//extraigo los datos del formulario y los guardo en la variable
          $.ajax({//indico a donde quiero enviar los datos
            url: "../class/classBundles.php?accion="+accion+"&id_bdl="+param,
            data: $datos,
            type: "POST"});
        $.confirm({title: 'Editando',
                 content: "Datos actualizados",
                 columnClass: 'xsmall',
                 type: 'orange',
                 buttons:{
                   aceptar: {
                     text: 'Aceptar',
                     action: function () {
                       bundless('list');
                     }
                   }
                 }
               });
          break;


          /*CASO PARA LA VISTA - bundles.php*/
          case 'mostrar':
          param=id_bdl;
          $.confirm({title: '',
                   content: "url: ../class/classBundles.php?accion="+accion+"&id_bdl="+id_bdl,
                   columnClass: 'large',
                   type: 'green',
                   buttons:{
                     aceptar: {
                       btnClass: 'btn-green',
                       text: 'Comprar',
                       action: function () {
                         bundless('venta');
                       }
                     },
                     cancelar: {
                       text: 'Cancelar',
                       action: function(){}
                     }
                   }
                 });
            break;

            case 'venta':
            $.confirm({title: '<h3>Resumen de pedido</h3>',
                     content: "url: ../class/classBundles.php?accion="+accion+"&id_bdl="+param,
                     columnClass: 'large',
                     type: 'green',
                     buttons:{
                       aceptar: {
                         btnClass: 'btn-green',
                         text: 'Confirmar',
                         action: function () {
                          bundless('comprar'); //agregar confirm con content: "url: ../class/classBundles.php?accion="+accion+"&id_bdl="+param,
                         }
                       },
                       cancelar: {
                         text: 'Cancelar',
                         action: function(){}
                       }
                     }
                   });
              break;
              case 'comprar':
                datos=$("#formVista").serialize();//extraigo los datos del formulario y los guardo en la variable
                  $.ajax({//indico a donde quiero enviar los datos
                    url: "../class/classBundles.php?accion="+accion+"&identi="+param,
                    data: datos,
                    type: "POST"});
                $.confirm({title: 'Transaccion finalizada',
                         content: "Articulo comprado",
                         columnClass: 'small',
                         type: 'orange',
                         buttons:{
                           aceptar: {
                             text: 'Aceptar',
                             action: function () {
                             }
                           }
                         }
                       });
                break;
    default: $.alert({title: "Atencion", content: accion + ": no ha sido programado"})
  }
}
